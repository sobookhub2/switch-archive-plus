# Archive Plus 
This script allows you to do exactlty the same function as the "Archive" element from Enfocus Switch plus the ability to flatten your archive.

## Usage
Send a job (file or folder) trough the element in order to ZIP it.


### Flow element properties

#### Compress
If set to Yes, the files in the ZIP archive are compressed (default behavior).
If set to No, the files are stored in the ZIP archive without compression, dramatically reducing the processing time; this can be meaningful when speed is more important than size, or when the files will not compress well anyway (for example, in case of JPEG images) – but you still want a single archive containing all files.

#### Password
The password used for protecting archives, or blank (if not password-protected).

### Remove top level Folder (folder only)
In case of file, this setting will have no impact.
In case of folder : 
- If set to yes, the folder won't be archive
- If set to no, the folder name will be kept at top level.

#### Debug verbose
Optional flag to send verbose debug messages to the log. Make sure log debugging is on: _Preferences > Logging > Log debug messages = Yes_.

## App
Portals is also available in the [Enfocus appstore](https://www.enfocus.com/en/appstore/product/archive-plus) as a free app.