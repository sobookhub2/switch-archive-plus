function debugLog(s, message) {
	const debugLog = s.getPropertyValue("debugLog") === "Yes";
	if (debugLog) {
		s.log(-1, message);
	}
}

function archiveJob(s, job, sourcePath, destinationPath, fileName) {
	const password = s.getPropertyValue("password");
	const compress = s.getPropertyValue("compress") === "Yes";
	const archiveResult = s.archive(sourcePath, destinationPath, password, compress, false, fileName);
	if (!archiveResult) {
		job.fail("Failed to archive.");
	}
	return archiveResult;
}

function jobArrived(s: Switch, job: Job) {
	const sourcePath = job.getPath();
	const destinationPath = s.createPathWithName(job.getNameProper() + ".zip", false);
	const removeTopLevelFolder = s.getPropertyValue("removeTopLevelFolder") === "Yes";
	debugLog(s, "remove top folder : " + removeTopLevelFolder);
	debugLog(s, "destinationPath : " + destinationPath);
	if (job.isFile()) {
		var archiveResult = archiveJob(s, job, sourcePath, destinationPath, job.getName());
	} else {
		const jobDir = new Dir(sourcePath);
		const entries = jobDir.entryList("*", Dir.All, Dir.Name);
		debugLog(s, "Files found: " + entries.length);
		if (!entries.length) {
			job.fail("Input folder must have file in it");
		}
		if (!removeTopLevelFolder) {
			var archiveResult = archiveJob(s, job, sourcePath, destinationPath, job.getName());
		} else {
			for (i = 0; i < entries.length; i++) {
				const fileName = entries[i];
				var archiveResult = archiveJob(s, job, jobDir.filePath(fileName), destinationPath, fileName);
				debugLog(s, "Picking up: " + fileName);
				debugLog(s, "Added to archive: " + fileName);
				debugLog(s, "archiveResult: " + archiveResult);
			}
		}
	}
	job.sendToSingle(destinationPath);
}
